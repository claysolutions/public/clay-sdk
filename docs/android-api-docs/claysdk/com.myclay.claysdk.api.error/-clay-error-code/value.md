//[claysdk](../../../index.md)/[com.myclay.claysdk.api.error](../index.md)/[ClayErrorCode](index.md)/[value](value.md)

# value

[androidJvm]\
val [value](value.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)
