//[claysdk](../../../../index.md)/[com.myclay.claysdk.api.error](../../index.md)/[ClayErrorCode](../index.md)/[COARSE_LOCATION_PERMISSION_DENIED](index.md)

# COARSE_LOCATION_PERMISSION_DENIED

[androidJvm]\
[COARSE_LOCATION_PERMISSION_DENIED](index.md)

Coarse location permission was denied - JustinError 501

## Properties

| Name | Summary |
|---|---|
| [name](../-j-u-s-t-i-n_-b-l-e_-i-n-i-t-i-a-l-i-z-a-t-i-o-n/index.md#-372974862%2FProperties%2F493929636) | [androidJvm]<br>val [name](../-j-u-s-t-i-n_-b-l-e_-i-n-i-t-i-a-l-i-z-a-t-i-o-n/index.md#-372974862%2FProperties%2F493929636): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [ordinal](../-j-u-s-t-i-n_-b-l-e_-i-n-i-t-i-a-l-i-z-a-t-i-o-n/index.md#-739389684%2FProperties%2F493929636) | [androidJvm]<br>val [ordinal](../-j-u-s-t-i-n_-b-l-e_-i-n-i-t-i-a-l-i-z-a-t-i-o-n/index.md#-739389684%2FProperties%2F493929636): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
| [value](../value.md) | [androidJvm]<br>val [value](../value.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
