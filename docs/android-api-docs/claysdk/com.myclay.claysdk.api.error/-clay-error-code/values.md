//[claysdk](../../../index.md)/[com.myclay.claysdk.api.error](../index.md)/[ClayErrorCode](index.md)/[values](values.md)

# values

[androidJvm]\
fun [values](values.md)(): [Array](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-array/index.html)&lt;[ClayErrorCode](index.md)&gt;

Returns an array containing the constants of this enum type, in the order they're declared.

This method may be used to iterate over the constants.
