//[claysdk](../../../../index.md)/[com.myclay.claysdk.api.error](../../index.md)/[ClayErrorCode](../index.md)/[JUSTIN_BLE_INITIALIZATION](index.md)

# JUSTIN_BLE_INITIALIZATION

[androidJvm]\
[JUSTIN_BLE_INITIALIZATION](index.md)

Justin BLE wasn't initialised properly because the app doesn't have bluetooth permissions for SDK 31

## Properties

| Name | Summary |
|---|---|
| [name](index.md#-372974862%2FProperties%2F493929636) | [androidJvm]<br>val [name](index.md#-372974862%2FProperties%2F493929636): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [ordinal](index.md#-739389684%2FProperties%2F493929636) | [androidJvm]<br>val [ordinal](index.md#-739389684%2FProperties%2F493929636): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
| [value](../value.md) | [androidJvm]<br>val [value](../value.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
