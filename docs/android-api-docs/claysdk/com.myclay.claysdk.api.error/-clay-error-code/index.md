//[claysdk](../../../index.md)/[com.myclay.claysdk.api.error](../index.md)/[ClayErrorCode](index.md)

# ClayErrorCode

[androidJvm]\
enum [ClayErrorCode](index.md) : [Enum](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-enum/index.html)&lt;[ClayErrorCode](index.md)&gt; 

A list of all the possible error codes a ClayException can have

## Entries

| | |
|---|---|
| [CONNECTION_GENERAL](-c-o-n-n-e-c-t-i-o-n_-g-e-n-e-r-a-l/index.md) | [androidJvm]<br>[CONNECTION_GENERAL](-c-o-n-n-e-c-t-i-o-n_-g-e-n-e-r-a-l/index.md)<br>General error during connection - JustinError JustinError 401 |
| [PROCESS_ALREADY_RUNNING](-p-r-o-c-e-s-s_-a-l-r-e-a-d-y_-r-u-n-n-i-n-g/index.md) | [androidJvm]<br>[PROCESS_ALREADY_RUNNING](-p-r-o-c-e-s-s_-a-l-r-e-a-d-y_-r-u-n-n-i-n-g/index.md)<br>Process is already running (when startUnlocking is called before timeout/success/failure) - JustinError 402 |
| [INCORRECT_DIGITAL_KEY_DATA](-i-n-c-o-r-r-e-c-t_-d-i-g-i-t-a-l_-k-e-y_-d-a-t-a/index.md) | [androidJvm]<br>[INCORRECT_DIGITAL_KEY_DATA](-i-n-c-o-r-r-e-c-t_-d-i-g-i-t-a-l_-k-e-y_-d-a-t-a/index.md)<br>The private key used to decrypt an encrypted digital key is invalid - JustinError 403 |
| [BLUETOOTH_NOT_SUPPORTED](-b-l-u-e-t-o-o-t-h_-n-o-t_-s-u-p-p-o-r-t-e-d/index.md) | [androidJvm]<br>[BLUETOOTH_NOT_SUPPORTED](-b-l-u-e-t-o-o-t-h_-n-o-t_-s-u-p-p-o-r-t-e-d/index.md)<br>Bluetooth is not supported by this device - JustinError 404 |
| [BLUETOOTH_FEATURE_NOT_ENABLED](-b-l-u-e-t-o-o-t-h_-f-e-a-t-u-r-e_-n-o-t_-e-n-a-b-l-e-d/index.md) | [androidJvm]<br>[BLUETOOTH_FEATURE_NOT_ENABLED](-b-l-u-e-t-o-o-t-h_-f-e-a-t-u-r-e_-n-o-t_-e-n-a-b-l-e-d/index.md)<br>Bluetooth is not enabled on the device's settings (possibly on Android Manifest permissions') - JustinError 405 |
| [BLUETOOTH_FEATURE_NOT_INITIALIZED](-b-l-u-e-t-o-o-t-h_-f-e-a-t-u-r-e_-n-o-t_-i-n-i-t-i-a-l-i-z-e-d/index.md) | [androidJvm]<br>[BLUETOOTH_FEATURE_NOT_INITIALIZED](-b-l-u-e-t-o-o-t-h_-f-e-a-t-u-r-e_-n-o-t_-i-n-i-t-i-a-l-i-z-e-d/index.md)<br>Bluetooth is not initialized yet - JustinError 406 |
| [DISCONNECTED_GATT_SERVER](-d-i-s-c-o-n-n-e-c-t-e-d_-g-a-t-t_-s-e-r-v-e-r/index.md) | [androidJvm]<br>[DISCONNECTED_GATT_SERVER](-d-i-s-c-o-n-n-e-c-t-e-d_-g-a-t-t_-s-e-r-v-e-r/index.md)<br>Disconnected from Gatt Server - JustinError 407 |
| [OPERATION_CANCELLED](-o-p-e-r-a-t-i-o-n_-c-a-n-c-e-l-l-e-d/index.md) | [androidJvm]<br>[OPERATION_CANCELLED](-o-p-e-r-a-t-i-o-n_-c-a-n-c-e-l-l-e-d/index.md)<br>Operation was cancelled - JustinError 408 |
| [NO_SERVICES_FOUND](-n-o_-s-e-r-v-i-c-e-s_-f-o-u-n-d/index.md) | [androidJvm]<br>[NO_SERVICES_FOUND](-n-o_-s-e-r-v-i-c-e-s_-f-o-u-n-d/index.md)<br>No services found on this phone - JustinError 409 |
| [TIMEOUT_REACHED](-t-i-m-e-o-u-t_-r-e-a-c-h-e-d/index.md) | [androidJvm]<br>[TIMEOUT_REACHED](-t-i-m-e-o-u-t_-r-e-a-c-h-e-d/index.md)<br>Timeout reached - JustinError 410 |
| [INVALID_SERVICE_CHARACTERISTICS](-i-n-v-a-l-i-d_-s-e-r-v-i-c-e_-c-h-a-r-a-c-t-e-r-i-s-t-i-c-s/index.md) | [androidJvm]<br>[INVALID_SERVICE_CHARACTERISTICS](-i-n-v-a-l-i-d_-s-e-r-v-i-c-e_-c-h-a-r-a-c-t-e-r-i-s-t-i-c-s/index.md)<br>Invalid service configuration - JustinError 411 |
| [INVALID_PROTOCOL_VERSION](-i-n-v-a-l-i-d_-p-r-o-t-o-c-o-l_-v-e-r-s-i-o-n/index.md) | [androidJvm]<br>[INVALID_PROTOCOL_VERSION](-i-n-v-a-l-i-d_-p-r-o-t-o-c-o-l_-v-e-r-s-i-o-n/index.md)<br>Invalid protocol version - JustinError 412 |
| [INVALID_DATA_RECEIVED](-i-n-v-a-l-i-d_-d-a-t-a_-r-e-c-e-i-v-e-d/index.md) | [androidJvm]<br>[INVALID_DATA_RECEIVED](-i-n-v-a-l-i-d_-d-a-t-a_-r-e-c-e-i-v-e-d/index.md)<br>Invalid data received - JustinError 413 |
| [AUTHENTICATION_FAILED](-a-u-t-h-e-n-t-i-c-a-t-i-o-n_-f-a-i-l-e-d/index.md) | [androidJvm]<br>[AUTHENTICATION_FAILED](-a-u-t-h-e-n-t-i-c-a-t-i-o-n_-f-a-i-l-e-d/index.md)<br>Authentication failed - JustinError 414 |
| [BLUETOOTH_NOT_AUTHORIZED](-b-l-u-e-t-o-o-t-h_-n-o-t_-a-u-t-h-o-r-i-z-e-d/index.md) | [androidJvm]<br>[BLUETOOTH_NOT_AUTHORIZED](-b-l-u-e-t-o-o-t-h_-n-o-t_-a-u-t-h-o-r-i-z-e-d/index.md)<br>Bluetooth is not authorized - JustinError 415 |
| [BLUETOOTH_SCANNING](-b-l-u-e-t-o-o-t-h_-s-c-a-n-n-i-n-g/index.md) | [androidJvm]<br>[BLUETOOTH_SCANNING](-b-l-u-e-t-o-o-t-h_-s-c-a-n-n-i-n-g/index.md)<br>Bluetooth scanning error - JustinError 416 |
| [BLUETOOTH_SERVICE_NOT_FOUND](-b-l-u-e-t-o-o-t-h_-s-e-r-v-i-c-e_-n-o-t_-f-o-u-n-d/index.md) | [androidJvm]<br>[BLUETOOTH_SERVICE_NOT_FOUND](-b-l-u-e-t-o-o-t-h_-s-e-r-v-i-c-e_-n-o-t_-f-o-u-n-d/index.md)<br>Bluetooth service not found - JustinError 500 |
| [COARSE_LOCATION_PERMISSION_DENIED](-c-o-a-r-s-e_-l-o-c-a-t-i-o-n_-p-e-r-m-i-s-s-i-o-n_-d-e-n-i-e-d/index.md) | [androidJvm]<br>[COARSE_LOCATION_PERMISSION_DENIED](-c-o-a-r-s-e_-l-o-c-a-t-i-o-n_-p-e-r-m-i-s-s-i-o-n_-d-e-n-i-e-d/index.md)<br>Coarse location permission was denied - JustinError 501 |
| [COARSE_LOCATION_NOT_ENABLED](-c-o-a-r-s-e_-l-o-c-a-t-i-o-n_-n-o-t_-e-n-a-b-l-e-d/index.md) | [androidJvm]<br>[COARSE_LOCATION_NOT_ENABLED](-c-o-a-r-s-e_-l-o-c-a-t-i-o-n_-n-o-t_-e-n-a-b-l-e-d/index.md)<br>Coarse location is not enabled (possibly permission) - JustinError 502 |
| [HCE_FEATURE_NOT_AVAILABLE](-h-c-e_-f-e-a-t-u-r-e_-n-o-t_-a-v-a-i-l-a-b-l-e/index.md) | [androidJvm]<br>[HCE_FEATURE_NOT_AVAILABLE](-h-c-e_-f-e-a-t-u-r-e_-n-o-t_-a-v-a-i-l-a-b-l-e/index.md)<br>NFC is not supported by this device - JustinError 503 |
| [FINE_LOCATION_PERMISSION_DENIED](-f-i-n-e_-l-o-c-a-t-i-o-n_-p-e-r-m-i-s-s-i-o-n_-d-e-n-i-e-d/index.md) | [androidJvm]<br>[FINE_LOCATION_PERMISSION_DENIED](-f-i-n-e_-l-o-c-a-t-i-o-n_-p-e-r-m-i-s-s-i-o-n_-d-e-n-i-e-d/index.md)<br>Fine location permission was denied - JustinError 504 |
| [FINE_LOCATION_NOT_ENABLED](-f-i-n-e_-l-o-c-a-t-i-o-n_-n-o-t_-e-n-a-b-l-e-d/index.md) | [androidJvm]<br>[FINE_LOCATION_NOT_ENABLED](-f-i-n-e_-l-o-c-a-t-i-o-n_-n-o-t_-e-n-a-b-l-e-d/index.md)<br>Fine location is not enabled (possibly permission) - JustinError 505 |
| [BLUETOOTH_SCAN_NOT_ENABLED](-b-l-u-e-t-o-o-t-h_-s-c-a-n_-n-o-t_-e-n-a-b-l-e-d/index.md) | [androidJvm]<br>[BLUETOOTH_SCAN_NOT_ENABLED](-b-l-u-e-t-o-o-t-h_-s-c-a-n_-n-o-t_-e-n-a-b-l-e-d/index.md)<br>Bluetooth scan is not enabled (possibly permission) - JustinError 506 |
| [BLUETOOTH_CONNECT_NOT_ENABLED](-b-l-u-e-t-o-o-t-h_-c-o-n-n-e-c-t_-n-o-t_-e-n-a-b-l-e-d/index.md) | [androidJvm]<br>[BLUETOOTH_CONNECT_NOT_ENABLED](-b-l-u-e-t-o-o-t-h_-c-o-n-n-e-c-t_-n-o-t_-e-n-a-b-l-e-d/index.md)<br>Bluetooth connect is not enabled (possibly permission) - JustinError 507 |
| [INTERNAL_ERROR](-i-n-t-e-r-n-a-l_-e-r-r-o-r/index.md) | [androidJvm]<br>[INTERNAL_ERROR](-i-n-t-e-r-n-a-l_-e-r-r-o-r/index.md)<br>Internal generic error (not within JustinErrorCodes and not part of our custom codes) |
| [INVALID_API_PUBLIC_KEY](-i-n-v-a-l-i-d_-a-p-i_-p-u-b-l-i-c_-k-e-y/index.md) | [androidJvm]<br>[INVALID_API_PUBLIC_KEY](-i-n-v-a-l-i-d_-a-p-i_-p-u-b-l-i-c_-k-e-y/index.md)<br>The API public key is invalid |
| [EMPTY_API_PUBLIC_KEY](-e-m-p-t-y_-a-p-i_-p-u-b-l-i-c_-k-e-y/index.md) | [androidJvm]<br>[EMPTY_API_PUBLIC_KEY](-e-m-p-t-y_-a-p-i_-p-u-b-l-i-c_-k-e-y/index.md)<br>The API public key is empty |
| [INVALID_PRIVATE_KEY](-i-n-v-a-l-i-d_-p-r-i-v-a-t-e_-k-e-y/index.md) | [androidJvm]<br>[INVALID_PRIVATE_KEY](-i-n-v-a-l-i-d_-p-r-i-v-a-t-e_-k-e-y/index.md)<br>The private key used to decrypt an encrypted digital key is invalid |
| [PRIVATE_KEY_IS_NULL](-p-r-i-v-a-t-e_-k-e-y_-i-s_-n-u-l-l/index.md) | [androidJvm]<br>[PRIVATE_KEY_IS_NULL](-p-r-i-v-a-t-e_-k-e-y_-i-s_-n-u-l-l/index.md)<br>The private key used for decryption is null |
| [PRIVATE_KEY_IS_EMPTY](-p-r-i-v-a-t-e_-k-e-y_-i-s_-e-m-p-t-y/index.md) | [androidJvm]<br>[PRIVATE_KEY_IS_EMPTY](-p-r-i-v-a-t-e_-k-e-y_-i-s_-e-m-p-t-y/index.md)<br>The private key used for decryption is empty |
| [INVALID_BASE64_DIGITAL_KEY](-i-n-v-a-l-i-d_-b-a-s-e64_-d-i-g-i-t-a-l_-k-e-y/index.md) | [androidJvm]<br>[INVALID_BASE64_DIGITAL_KEY](-i-n-v-a-l-i-d_-b-a-s-e64_-d-i-g-i-t-a-l_-k-e-y/index.md)<br>The BASE64 encoding of the digital key is invalid |
| [INVALID_DIGITAL_KEY](-i-n-v-a-l-i-d_-d-i-g-i-t-a-l_-k-e-y/index.md) | [androidJvm]<br>[INVALID_DIGITAL_KEY](-i-n-v-a-l-i-d_-d-i-g-i-t-a-l_-k-e-y/index.md)<br>The supplied digital key has an invalid format |
| [DIGITAL_KEY_DECRYPT_FAILED](-d-i-g-i-t-a-l_-k-e-y_-d-e-c-r-y-p-t_-f-a-i-l-e-d/index.md) | [androidJvm]<br>[DIGITAL_KEY_DECRYPT_FAILED](-d-i-g-i-t-a-l_-k-e-y_-d-e-c-r-y-p-t_-f-a-i-l-e-d/index.md)<br>Decryption of the digital key did not succeed |
| [JUSTIN_BLE_INITIALIZATION](-j-u-s-t-i-n_-b-l-e_-i-n-i-t-i-a-l-i-z-a-t-i-o-n/index.md) | [androidJvm]<br>[JUSTIN_BLE_INITIALIZATION](-j-u-s-t-i-n_-b-l-e_-i-n-i-t-i-a-l-i-z-a-t-i-o-n/index.md)<br>Justin BLE wasn't initialised properly because the app doesn't have bluetooth permissions for SDK 31 |

## Types

| Name | Summary |
|---|---|
| [Companion](-companion/index.md) | [androidJvm]<br>object [Companion](-companion/index.md) |

## Functions

| Name | Summary |
|---|---|
| [valueOf](value-of.md) | [androidJvm]<br>fun [valueOf](value-of.md)(value: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): [ClayErrorCode](index.md)<br>Returns the enum constant of this type with the specified name. The string must match exactly an identifier used to declare an enum constant in this type. (Extraneous whitespace characters are not permitted.) |
| [values](values.md) | [androidJvm]<br>fun [values](values.md)(): [Array](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-array/index.html)&lt;[ClayErrorCode](index.md)&gt;<br>Returns an array containing the constants of this enum type, in the order they're declared. |

## Properties

| Name | Summary |
|---|---|
| [name](-j-u-s-t-i-n_-b-l-e_-i-n-i-t-i-a-l-i-z-a-t-i-o-n/index.md#-372974862%2FProperties%2F493929636) | [androidJvm]<br>val [name](-j-u-s-t-i-n_-b-l-e_-i-n-i-t-i-a-l-i-z-a-t-i-o-n/index.md#-372974862%2FProperties%2F493929636): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [ordinal](-j-u-s-t-i-n_-b-l-e_-i-n-i-t-i-a-l-i-z-a-t-i-o-n/index.md#-739389684%2FProperties%2F493929636) | [androidJvm]<br>val [ordinal](-j-u-s-t-i-n_-b-l-e_-i-n-i-t-i-a-l-i-z-a-t-i-o-n/index.md#-739389684%2FProperties%2F493929636): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
| [value](value.md) | [androidJvm]<br>val [value](value.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
