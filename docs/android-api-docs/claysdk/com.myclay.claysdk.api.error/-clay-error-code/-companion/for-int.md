//[claysdk](../../../../index.md)/[com.myclay.claysdk.api.error](../../index.md)/[ClayErrorCode](../index.md)/[Companion](index.md)/[forInt](for-int.md)

# forInt

[androidJvm]\
fun [forInt](for-int.md)(errorCode: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)): [ClayErrorCode](../index.md)
