//[claysdk](../../../../index.md)/[com.myclay.claysdk.api.error](../../index.md)/[ClayErrorCode](../index.md)/[Companion](index.md)

# Companion

[androidJvm]\
object [Companion](index.md)

## Functions

| Name | Summary |
|---|---|
| [forInt](for-int.md) | [androidJvm]<br>fun [forInt](for-int.md)(errorCode: [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)): [ClayErrorCode](../index.md) |
| [message](message.md) | [androidJvm]<br>fun [message](message.md)(errorCode: [ClayErrorCode](../index.md)): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
