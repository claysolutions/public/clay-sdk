//[claysdk](../../../../index.md)/[com.myclay.claysdk.api.error](../../index.md)/[ClayErrorCode](../index.md)/[Companion](index.md)/[message](message.md)

# message

[androidJvm]\
fun [message](message.md)(errorCode: [ClayErrorCode](../index.md)): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)
