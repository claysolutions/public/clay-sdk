//[claysdk](../../../index.md)/[com.myclay.claysdk.api.error](../index.md)/[ClayException](index.md)/[errorCode](error-code.md)

# errorCode

[androidJvm]\
var [errorCode](error-code.md): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html)

Setter for the errorCode property

#### Parameters

androidJvm

| | |
|---|---|
| errorCode | The error code of the exception. |
