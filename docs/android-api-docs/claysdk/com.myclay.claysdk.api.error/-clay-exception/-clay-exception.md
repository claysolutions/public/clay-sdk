//[claysdk](../../../index.md)/[com.myclay.claysdk.api.error](../index.md)/[ClayException](index.md)/[ClayException](-clay-exception.md)

# ClayException

[androidJvm]\
fun [ClayException](-clay-exception.md)(errorCode: [ClayErrorCode](../-clay-error-code/index.md))

Creates a new ClayException using the error code supplied as a parameter. The message is inferred internally

#### Parameters

androidJvm

| | |
|---|---|
| errorCode | The error code of the exception |

[androidJvm]\
fun [ClayException](-clay-exception.md)(exception: JustinException)

Creates a new ClayException based on a JustinException

#### Parameters

androidJvm

| | |
|---|---|
| exception | The JustinException used to construct this ClayException |
