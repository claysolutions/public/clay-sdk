//[claysdk](../../index.md)/[com.myclay.claysdk.api.error](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [ClayErrorCode](-clay-error-code/index.md) | [androidJvm]<br>enum [ClayErrorCode](-clay-error-code/index.md) : [Enum](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-enum/index.html)&lt;[ClayErrorCode](-clay-error-code/index.md)&gt; <br>A list of all the possible error codes a ClayException can have |
| [ClayException](-clay-exception/index.md) | [androidJvm]<br>class [ClayException](-clay-exception/index.md) : [Exception](https://developer.android.com/reference/kotlin/java/lang/Exception.html)<br>The model class for the exceptions thrown by the library |
