//[claysdk](../../../index.md)/[com.myclay.claysdk.api](../index.md)/[IDigitalKeyCallback](index.md)

# IDigitalKeyCallback

[androidJvm]\
interface [IDigitalKeyCallback](index.md)

## Functions

| Name | Summary |
|---|---|
| [onFailure](on-failure.md) | [androidJvm]<br>abstract fun [onFailure](on-failure.md)(exception: [ClayException](../../com.myclay.claysdk.api.error/-clay-exception/index.md))<br>Callback method when the opening operation has failed |
| [onLockFound](on-lock-found.md) | [androidJvm]<br>abstract fun [onLockFound](on-lock-found.md)()<br>Callback method called when a peripheral/lock is found |
| [onSuccess](on-success.md) | [androidJvm]<br>abstract fun [onSuccess](on-success.md)(result: [ClayResult](../-clay-result/index.md), message: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?)<br>Callback method when the opening operation has succeeded |
