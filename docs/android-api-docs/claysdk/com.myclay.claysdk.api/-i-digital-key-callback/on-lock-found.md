//[claysdk](../../../index.md)/[com.myclay.claysdk.api](../index.md)/[IDigitalKeyCallback](index.md)/[onLockFound](on-lock-found.md)

# onLockFound

[androidJvm]\
abstract fun [onLockFound](on-lock-found.md)()

Callback method called when a peripheral/lock is found
