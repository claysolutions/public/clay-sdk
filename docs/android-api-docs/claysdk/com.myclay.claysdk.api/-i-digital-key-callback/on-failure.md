//[claysdk](../../../index.md)/[com.myclay.claysdk.api](../index.md)/[IDigitalKeyCallback](index.md)/[onFailure](on-failure.md)

# onFailure

[androidJvm]\
abstract fun [onFailure](on-failure.md)(exception: [ClayException](../../com.myclay.claysdk.api.error/-clay-exception/index.md))

Callback method when the opening operation has failed

#### Parameters

androidJvm

| | |
|---|---|
| exception | The ClayException caught and returned for processing |
