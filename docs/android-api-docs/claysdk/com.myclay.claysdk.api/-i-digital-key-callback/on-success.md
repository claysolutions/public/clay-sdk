//[claysdk](../../../index.md)/[com.myclay.claysdk.api](../index.md)/[IDigitalKeyCallback](index.md)/[onSuccess](on-success.md)

# onSuccess

[androidJvm]\
abstract fun [onSuccess](on-success.md)(result: [ClayResult](../-clay-result/index.md), message: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)?)

Callback method when the opening operation has succeeded

#### Parameters

androidJvm

| | |
|---|---|
| result | The opening Result |
