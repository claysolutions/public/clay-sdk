//[claysdk](../../../index.md)/[com.myclay.claysdk.api](../index.md)/[ClayResult](index.md)

# ClayResult

[androidJvm]\
enum [ClayResult](index.md) : [Enum](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-enum/index.html)&lt;[ClayResult](index.md)&gt;

## Entries

| | |
|---|---|
| [SUCCESS](-s-u-c-c-e-s-s/index.md) | [androidJvm]<br>[SUCCESS](-s-u-c-c-e-s-s/index.md) |
| [FAILURE](-f-a-i-l-u-r-e/index.md) | [androidJvm]<br>[FAILURE](-f-a-i-l-u-r-e/index.md) |
| [CANCELLED](-c-a-n-c-e-l-l-e-d/index.md) | [androidJvm]<br>[CANCELLED](-c-a-n-c-e-l-l-e-d/index.md) |

## Functions

| Name | Summary |
|---|---|
| [valueOf](value-of.md) | [androidJvm]<br>fun [valueOf](value-of.md)(value: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): [ClayResult](index.md)<br>Returns the enum constant of this type with the specified name. The string must match exactly an identifier used to declare an enum constant in this type. (Extraneous whitespace characters are not permitted.) |
| [values](values.md) | [androidJvm]<br>fun [values](values.md)(): [Array](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-array/index.html)&lt;[ClayResult](index.md)&gt;<br>Returns an array containing the constants of this enum type, in the order they're declared. |

## Properties

| Name | Summary |
|---|---|
| [name](../../com.myclay.claysdk.api.error/-clay-error-code/-j-u-s-t-i-n_-b-l-e_-i-n-i-t-i-a-l-i-z-a-t-i-o-n/index.md#-372974862%2FProperties%2F493929636) | [androidJvm]<br>val [name](../../com.myclay.claysdk.api.error/-clay-error-code/-j-u-s-t-i-n_-b-l-e_-i-n-i-t-i-a-l-i-z-a-t-i-o-n/index.md#-372974862%2FProperties%2F493929636): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [ordinal](../../com.myclay.claysdk.api.error/-clay-error-code/-j-u-s-t-i-n_-b-l-e_-i-n-i-t-i-a-l-i-z-a-t-i-o-n/index.md#-739389684%2FProperties%2F493929636) | [androidJvm]<br>val [ordinal](../../com.myclay.claysdk.api.error/-clay-error-code/-j-u-s-t-i-n_-b-l-e_-i-n-i-t-i-a-l-i-z-a-t-i-o-n/index.md#-739389684%2FProperties%2F493929636): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
