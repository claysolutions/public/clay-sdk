//[claysdk](../../../../index.md)/[com.myclay.claysdk.api](../../index.md)/[ClayResult](../index.md)/[SUCCESS](index.md)

# SUCCESS

[androidJvm]\
[SUCCESS](index.md)

## Properties

| Name | Summary |
|---|---|
| [name](../../../com.myclay.claysdk.api.error/-clay-error-code/-j-u-s-t-i-n_-b-l-e_-i-n-i-t-i-a-l-i-z-a-t-i-o-n/index.md#-372974862%2FProperties%2F493929636) | [androidJvm]<br>val [name](../../../com.myclay.claysdk.api.error/-clay-error-code/-j-u-s-t-i-n_-b-l-e_-i-n-i-t-i-a-l-i-z-a-t-i-o-n/index.md#-372974862%2FProperties%2F493929636): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html) |
| [ordinal](../../../com.myclay.claysdk.api.error/-clay-error-code/-j-u-s-t-i-n_-b-l-e_-i-n-i-t-i-a-l-i-z-a-t-i-o-n/index.md#-739389684%2FProperties%2F493929636) | [androidJvm]<br>val [ordinal](../../../com.myclay.claysdk.api.error/-clay-error-code/-j-u-s-t-i-n_-b-l-e_-i-n-i-t-i-a-l-i-z-a-t-i-o-n/index.md#-739389684%2FProperties%2F493929636): [Int](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-int/index.html) |
