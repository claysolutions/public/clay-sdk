//[claysdk](../../../index.md)/[com.myclay.claysdk.api](../index.md)/[IClaySDK](index.md)/[publicKey](public-key.md)

# publicKey

[androidJvm]\
abstract val [publicKey](public-key.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)

Returns the BASE64 encoding of the PublicKey generated in the initialization step of the SDK

#### Return

BASE64 string encoding of the PublicKey

#### Throws

| | |
|---|---|
| [ClayException](../../com.myclay.claysdk.api.error/-clay-exception/index.md) | An exception when the getPublicKey operation fails |
