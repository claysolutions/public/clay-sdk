//[claysdk](../../../index.md)/[com.myclay.claysdk.api](../index.md)/[IClaySDK](index.md)/[sendDigitalKey](send-digital-key.md)

# sendDigitalKey

[androidJvm]\
abstract fun [sendDigitalKey](send-digital-key.md)(key: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), callback: [IDigitalKeyCallback](../-i-digital-key-callback/index.md))

Sends a command to open a door using the provided DigitalKey - deprecated, this method only enables BLE opening. For opening with NFC and/or BLE, use function with method

#### Parameters

androidJvm

| | |
|---|---|
| key | The DigitalKey which should be used to open a door |
| callback | A callback to notify the client about the result of the operation |

#### Throws

| | |
|---|---|
| [ClayException](../../com.myclay.claysdk.api.error/-clay-exception/index.md) | Exception thrown when the operation fails |
