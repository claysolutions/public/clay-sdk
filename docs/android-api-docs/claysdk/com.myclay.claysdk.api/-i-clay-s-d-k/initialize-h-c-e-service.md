//[claysdk](../../../index.md)/[com.myclay.claysdk.api](../index.md)/[IClaySDK](index.md)/[initializeHCEService](initialize-h-c-e-service.md)

# initializeHCEService

[androidJvm]\
abstract fun [initializeHCEService](initialize-h-c-e-service.md)(key: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), callback: [IDigitalKeyCallback](../-i-digital-key-callback/index.md))

Sends a command to open a door using the provided DigitalKey

#### Parameters

androidJvm

| | |
|---|---|
| key | The DigitalKey which should be used to open a door |
| callback | A callback to notify the client about the result of the operation |

#### Throws

| | |
|---|---|
| [ClayException](../../com.myclay.claysdk.api.error/-clay-exception/index.md) | Exception thrown when the operation fails |
