//[claysdk](../../../index.md)/[com.myclay.claysdk.api](../index.md)/[IClaySDK](index.md)

# IClaySDK

[androidJvm]\
interface [IClaySDK](index.md)

An interface defining the behavior of the library.

## Functions

| Name | Summary |
|---|---|
| [initializeHCEService](initialize-h-c-e-service.md) | [androidJvm]<br>abstract fun [initializeHCEService](initialize-h-c-e-service.md)(key: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), callback: [IDigitalKeyCallback](../-i-digital-key-callback/index.md))<br>Sends a command to open a door using the provided DigitalKey |
| [sendDigitalKey](send-digital-key.md) | [androidJvm]<br>abstract fun [sendDigitalKey](send-digital-key.md)(key: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), callback: [IDigitalKeyCallback](../-i-digital-key-callback/index.md))<br>Sends a command to open a door using the provided DigitalKey - deprecated, this method only enables BLE opening. For opening with NFC and/or BLE, use function with method |

## Properties

| Name | Summary |
|---|---|
| [publicKey](public-key.md) | [androidJvm]<br>abstract val [publicKey](public-key.md): [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)<br>Returns the BASE64 encoding of the PublicKey generated in the initialization step of the SDK |

## Inheritors

| Name |
|---|
| [ClaySDK](../../com.myclay.claysdk.internal/-clay-s-d-k/index.md) |
