//[claysdk](../../index.md)/[com.myclay.claysdk.api](index.md)

# Package-level declarations

## Types

| Name | Summary |
|---|---|
| [ClayResult](-clay-result/index.md) | [androidJvm]<br>enum [ClayResult](-clay-result/index.md) : [Enum](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-enum/index.html)&lt;[ClayResult](-clay-result/index.md)&gt; |
| [ClaySDK](-clay-s-d-k/index.md) | [androidJvm]<br>class [ClaySDK](-clay-s-d-k/index.md)<br>The point of entry in the library. Use this class to obtain an instance of IClaySDK and use it from that point onward to interact with the ClaySDK |
| [IClaySDK](-i-clay-s-d-k/index.md) | [androidJvm]<br>interface [IClaySDK](-i-clay-s-d-k/index.md)<br>An interface defining the behavior of the library. |
| [IDigitalKeyCallback](-i-digital-key-callback/index.md) | [androidJvm]<br>interface [IDigitalKeyCallback](-i-digital-key-callback/index.md) |

## Functions

| Name | Summary |
|---|---|
| [mapToClayResult](map-to-clay-result.md) | [androidJvm]<br>fun JustinResult.[mapToClayResult](map-to-clay-result.md)(): [ClayResult](-clay-result/index.md)? |
