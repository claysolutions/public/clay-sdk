//[claysdk](../../../index.md)/[com.myclay.claysdk.api](../index.md)/[ClaySDK](index.md)

# ClaySDK

[androidJvm]\
class [ClaySDK](index.md)

The point of entry in the library. Use this class to obtain an instance of IClaySDK and use it from that point onward to interact with the ClaySDK

## Types

| Name | Summary |
|---|---|
| [Companion](-companion/index.md) | [androidJvm]<br>object [Companion](-companion/index.md) |
