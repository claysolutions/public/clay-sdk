//[claysdk](../../../../index.md)/[com.myclay.claysdk.api](../../index.md)/[ClaySDK](../index.md)/[Companion](index.md)/[init](init.md)

# init

[androidJvm]\

@[JvmStatic](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.jvm/-jvm-static/index.html)

fun [init](init.md)(context: [Context](https://developer.android.com/reference/kotlin/android/content/Context.html), apiKey: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), installationUID: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): [IClaySDK](../../-i-clay-s-d-k/index.md)?

Static initializer of the library. It creates an IClaySDK instance with which the client consuming the library should interact from that point onwards

#### Return

An instance of IClaySDK providing the behavior to interact with the library

#### Parameters

androidJvm

| | |
|---|---|
| context | The Context in which the library is being used |
| apiKey | the Public Key of Tenant consuming the SDK |
| installationUID | Unique installation Identifier. Used to store a unique `ClayKeypair` in the Shared Preferences. |
