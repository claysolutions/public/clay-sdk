//[claysdk](../../../../index.md)/[com.myclay.claysdk.api](../../index.md)/[ClaySDK](../index.md)/[Companion](index.md)

# Companion

[androidJvm]\
object [Companion](index.md)

## Functions

| Name | Summary |
|---|---|
| [init](init.md) | [androidJvm]<br>@[JvmStatic](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.jvm/-jvm-static/index.html)<br>fun [init](init.md)(context: [Context](https://developer.android.com/reference/kotlin/android/content/Context.html), apiKey: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html), installationUID: [String](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin/-string/index.html)): [IClaySDK](../../-i-clay-s-d-k/index.md)?<br>Static initializer of the library. It creates an IClaySDK instance with which the client consuming the library should interact from that point onwards |
