# ClaySDK Demo Applications

This repository contains Demo apps for Android and iOS. To find out more please check setup guide in Demo apps folders.

To access ClaySDK documentation click Wiki on side bar or [this link](https://gitlab.com/claysolutions/public/clay-sdk/-/wikis/home).

By [Salto KS](https://saltoks.com/).