package com.salto.claysdkdemo.application

import com.salto.claysdkdemo.modules.AppModule
import com.salto.claysdkdemo.modules.PresenterModule
import com.salto.claysdkdemo.modules.ServiceModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        PresenterModule::class,
        ServiceModule::class
    ]
)
interface ClayComponent : AppComponent {

    object Initializer {

        fun init(
            application: App
        ): ClayComponent {
            return DaggerClayComponent.builder()
                .appModule(AppModule(application))
                .presenterModule(PresenterModule())
                .serviceModule(ServiceModule())
                .build()
        }
    }
}