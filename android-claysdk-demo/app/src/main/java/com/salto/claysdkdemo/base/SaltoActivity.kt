package com.salto.claysdkdemo.base

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.salto.claysdkdemo.R
import com.salto.claysdkdemo.application.App
import javax.inject.Inject

abstract class SaltoActivity<V: IBasePresenter.View, P: IBasePresenter.Action<V>>: AppCompatActivity() {

    @Inject
    lateinit var presenter: P

    var app: App? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (application as? App)?.let {
            this.app = it
        }
        inject()
        bindView()
    }

    abstract fun inject()

    abstract fun bindView()

    open fun showMessageDialog(message: String) {
        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder
                .setMessage(message)
                .setPositiveButton(getString(R.string.ok)) { dialogInterface, _ ->
                    run {
                        dialogInterface.cancel()
                    }
                }
        val alertDialog = alertDialogBuilder.create()
        alertDialog.setCancelable(true)
        alertDialog.show()
    }
}