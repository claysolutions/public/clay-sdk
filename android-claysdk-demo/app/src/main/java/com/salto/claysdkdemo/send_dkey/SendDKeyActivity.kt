package com.salto.claysdkdemo.send_dkey

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.core.app.ActivityCompat
import com.myclay.claysdk.api.error.ClayException
import com.salto.claysdkdemo.R
import com.salto.claysdkdemo.base.SaltoActivity
import com.salto.claysdkdemo.databinding.ActivityMainBinding
import com.salto.claysdkdemo.enums.MKActivationState
import com.salto.claysdkdemo.main.MainActivity
import com.salto.claysdkdemo.main.presenters.IMainPresenter
import com.salto.claysdkdemo.models.GuestDigitalKey
import javax.inject.Inject

class SendDKeyActivity : SaltoActivity<IMainPresenter.View, IMainPresenter.Action>(),
    IMainPresenter.View {

    @Inject
    lateinit var handler: Handler

    lateinit var binding: ActivityMainBinding

    private var gdKey: GuestDigitalKey? = null

    private var status: String = ""
        set(value) {
            field = value
            binding.statusTv.apply {
                visibility = if (value.isEmpty()) GONE else VISIBLE
                text = value
            }
        }


    private fun setContentView() {
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView()

        binding.apply {
            (intent?.extras?.get("GDKEY") as GuestDigitalKey?)?.let {
                this@SendDKeyActivity.gdKey = it
                useNormalDkey.visibility = VISIBLE
            }
            openButton.apply {
                visibility = VISIBLE
                setOnClickListener {
                    openLock()
                }
            }
            useNormalDkey.setOnClickListener {
                Intent(this@SendDKeyActivity, MainActivity::class.java).apply {
                    addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    startActivity(this)
                }
            }
            logoutButton.visibility = GONE
            retryButton.visibility = GONE

        }
    }

    override fun inject() {
        app?.clayComponent?.inject(this)
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    private fun openLock() {
        showProgress()
        status = getString(R.string.looking_for_looks)
        binding.openButton.visibility = GONE
        presenter.openLock(key = gdKey)
    }

    override fun bindView() {
        presenter.bindView(this)
    }

    override fun requestMissingPermissions(permissions: Array<String>, requestCode: Int) {
        ActivityCompat.requestPermissions(this, permissions, requestCode)
    }

    override fun onPermissionGranted() {
        openLock()
    }

    override fun onNeverAskLocationPermissionAgain() {
        status = getString(R.string.settings_location_permissions)
        binding.apply {
            openButton.visibility = GONE
            progressBar.visibility = GONE
            settingButton.apply {
                settingButton.visibility = VISIBLE

                setOnClickListener {
                    val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                    val uri = Uri.fromParts("package", packageName, null)
                    intent.data = uri
                    startActivity(intent)
                    visibility = GONE
                    reset()
                    presenter.onGoToSettingsClick()
                }
            }
        }
    }

    override fun onPeripheralFound() {
        status = getString(R.string.lock_found)
    }

    override fun onSuccessWithCancelledKey() = Unit

    private fun reactivateDevice() = Unit

    override fun onKeySuccessfullySent() {
        status = getString(R.string.mobile_key_received_by_lock)
        reset()
    }

    private fun reset(delay: Long = 3000) {
        hideProgress()
        binding.openButton.visibility = VISIBLE
        handler.postDelayed({
            status = ""
        }, delay)
    }

    override fun onMKeyDecryptionFailed() = Unit

    override fun onBluetoothStatusChanged(enabled: Boolean) {
        if (enabled) {
            openLock()
            return
        }
        status = getString(R.string.required_to_mobile_key)
    }

    override fun onKeySendError(errorMessage: String, exception: ClayException) {
        status = errorMessage
        reset()
    }

    override fun onTimeOut() {
        status = getString(R.string.lock_not_found)
        reset()
    }

    override fun onMobileKeyNotFound() {
        reactivateDevice()
    }

    override fun onMissingLocationPermission() {
        status = getString(R.string.mkey_coarse_location_permission)
        reset()
    }

    override fun onActivationStateChanged(state: MKActivationState?) = Unit

    private fun onRegistrationError() = Unit

    private fun showProgress() {
        binding.progressBar.visibility = VISIBLE
    }

    private fun hideProgress() {
        binding.progressBar.visibility = GONE
    }

    private fun onDeviceActivated() = Unit

    private fun onMobileKeyDownload() = Unit

    private fun onDeviceRegistration() = Unit

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        presenter.onRequestPermissionResult(requestCode, permissions, grantResults, this)
    }
}