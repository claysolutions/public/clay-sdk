package com.salto.claysdkdemo.application

import com.salto.claysdkdemo.access_code.AccessCodeActivity
import com.salto.claysdkdemo.guest_digital_key.GuestDigitalKeysListActivity
import com.salto.claysdkdemo.login.LoginActivity
import com.salto.claysdkdemo.main.MainActivity
import com.salto.claysdkdemo.send_dkey.SendDKeyActivity
import com.salto.claysdkdemo.widget.AppWidgetPermissionActivity
import com.salto.claysdkdemo.widget.DemoWidgetProvider


interface AppComponent {

    fun inject(app: App)
    fun inject(widgetProvider: DemoWidgetProvider)

    fun inject(activity: LoginActivity)

    fun inject(activity: MainActivity)

    fun inject(activity: SendDKeyActivity)

    fun inject(activity: GuestDigitalKeysListActivity)

    fun inject(activity: AccessCodeActivity)

    fun inject(activity: AppWidgetPermissionActivity)
}