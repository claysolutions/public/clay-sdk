package com.salto.claysdkdemo.main.presenters

import android.Manifest
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Handler
import androidx.annotation.RequiresApi
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import com.myclay.claysdk.api.ClayResult
import com.myclay.claysdk.api.ClaySDK
import com.myclay.claysdk.api.IClaySDK
import com.myclay.claysdk.api.IDigitalKeyCallback
import com.myclay.claysdk.api.error.ClayErrorCode
import com.myclay.claysdk.api.error.ClayException
import com.salto.claysdkdemo.R
import com.salto.claysdkdemo.application.AppConfig
import com.salto.claysdkdemo.application.ISharedPrefsUtil
import com.salto.claysdkdemo.base.BasePresenter
import com.salto.claysdkdemo.enums.MKActivationState
import com.salto.claysdkdemo.listeners.IDeviceServiceListener
import com.salto.claysdkdemo.login.oid.IOIDConfig
import com.salto.claysdkdemo.models.Device
import com.salto.claysdkdemo.models.GuestDigitalKey
import com.salto.claysdkdemo.services.IDeviceService
import com.salto.claysdkdemo.utils.DeviceInfo
import net.openid.appauth.AppAuthConfiguration
import net.openid.appauth.AuthState
import net.openid.appauth.browser.BrowserSelector


class MainPresenter(
    context: Context,
    sharedPrefs: ISharedPrefsUtil,
    private val authState: AuthState,
    private val oidConfig: IOIDConfig,
    private val deviceService: IDeviceService,
    private val apiKey: String,
    private val claySDK: IClaySDK?,
    private val handler: Handler
) : BasePresenter<IMainPresenter.View>(context, sharedPrefs), IMainPresenter.Action,
    IDeviceServiceListener {

    private var isBluetoothTurnedOn: Boolean = false
    private var isOpening: Boolean = false
    private var isLocationPermissionDenied = false

    init {
        deviceService.setMKeyActivationServiceListener(this)
    }

    private var activationState: MKActivationState? = null
        set(value) {
            field = value
            view?.onActivationStateChanged(value)
        }

    private val lockDiscoveryCallback = object : IDigitalKeyCallback {

        /**
         * Called when a lock is detected
         */
        override fun onLockFound() {
            view?.onPeripheralFound()
            isOpening = false
        }

        /**
         * Called when the Mobile Key is correctly sent to the lock
         * Optionally result could give info about the opening operation
         */
        override fun onSuccess(result: ClayResult, message: String?) {
            isOpening = false
            if (result == ClayResult.CANCELLED) {
                view?.onSuccessWithCancelledKey()
                return
            }

            view?.onKeySuccessfullySent()

            when (result) {

                ClayResult.SUCCESS -> view?.onKeyAccepted()

                else -> view?.onKeyRejectedOrFailing()
            }

        }

        override fun onFailure(exception: ClayException) {
            handleClayException(exception)
        }
    }

    private val bluetoothReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            val action: String? = intent?.action
            if (action == BluetoothAdapter.ACTION_STATE_CHANGED) {
                when (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR)) {
                    BluetoothAdapter.STATE_TURNING_ON -> {
                        isBluetoothTurnedOn = true
                        scanOnBluetoothTurnedOn(AppConfig.Timers.MKEY_BLUETOOTH_ON_RETRY_DOUBLE)
                    }
                }
            }
        }
    }

    private fun scanOnBluetoothTurnedOn(delay: Long) {
        handler.postDelayed({
            view?.onBluetoothStatusChanged(true)
        }, delay)
    }

    private fun handleClayException(exception: ClayException) {
        isOpening = false
        when (exception.errorCode) {

            ClayErrorCode.DIGITAL_KEY_DECRYPT_FAILED.value -> view?.onMKeyDecryptionFailed()

            ClayErrorCode.BLUETOOTH_FEATURE_NOT_INITIALIZED.value -> view?.onBluetoothStatusChanged(
                false
            )

            ClayErrorCode.PROCESS_ALREADY_RUNNING.value -> forceNewOpening()

            ClayErrorCode.TIMEOUT_REACHED.value -> view?.onTimeOut()

            else -> view?.onKeySendError(getErrorMessage(exception), exception)

        }
    }

    private fun getErrorMessage(exception: ClayException): String {
        if (exception.errorCode == ClayErrorCode.COARSE_LOCATION_PERMISSION_DENIED.value) {
            return context.getString(R.string.mkey_coarse_location_permission)
        }
        if (isLocationNotEnabledError(exception)) {
            return context.getString(R.string.mkey_coarse_location_not_enabled)
        }
        return exception.localizedMessage ?: ""
    }

    private fun isLocationNotEnabledError(exception: ClayException): Boolean {
        return exception.errorCode == ClayErrorCode.COARSE_LOCATION_NOT_ENABLED.value ||
                exception.errorCode == ClayErrorCode.FINE_LOCATION_NOT_ENABLED.value
    }

    private fun forceNewOpening() {
        isOpening = false
        handler.postDelayed({ openLock() }, 200)
    }

    override fun logout() {
        val request: String = oidConfig.getLogoutRequest(authState)
        val intent: Intent = buildLogoutIntent(request)
        sharedPrefs.deleteAuthState()
        view?.startLogoutIntent(intent)
    }

    override fun checkOrRegisterDevice(): Boolean {
        sharedPrefs.device ?: run {
            getDeviceList()
            return false
        }
        return true
    }

    private fun getDeviceList() {
        activationState = MKActivationState.REGISTERING
        deviceService.getDeviceList(
            AppConfig.ApiParams.ODATA_FILTER_DEVICE_UUID
                .replace(
                    AppConfig.ApiParams.DEVICE_UUID_PLACE_HOLDER,
                    DeviceInfo.uniquePseudoID(context)
                )
        )
    }

    override fun registerDevice() {
        deleteDevice()
        getDeviceList()
    }

    private fun buildLogoutIntent(request: String): Intent {
        val intent = Intent(Intent.ACTION_VIEW)
        intent.setPackage(
            BrowserSelector.select(
                context,
                AppAuthConfiguration.DEFAULT.browserMatcher
            )?.packageName
        )
        intent.data = Uri.parse(request)
        intent.putExtra(CustomTabsIntent.EXTRA_TITLE_VISIBILITY_STATE, CustomTabsIntent.NO_TITLE)
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
        return intent
    }

    override fun onDeviceRegistered(device: Device?) {
        activationState = MKActivationState.DOWNLOADING
        device?.id?.let {
            downloadMKey(it)
        } ?: run { activationState = MKActivationState.ERROR }
    }

    override fun onDevice(device: Device?) {
        activationState = MKActivationState.ACTIVATED
        sharedPrefs.device = device
    }

    override fun onDeviceList(deviceList: List<Device>?) {
        if (deviceList.isNullOrEmpty()) {
            register()
            return
        }
        deviceList.firstOrNull { device -> device.deviceUid == DeviceInfo.uniquePseudoID(context) }
            ?.let { device ->
                putDeviceCertificate(device.id)
            } ?: register()
    }

    private fun putDeviceCertificate(deviceId: String) {
        try {
            claySDK?.publicKey?.let { publicKey ->
                deviceService.putDeviceCertificate(deviceId, publicKey)
            }
        } catch (e: ClayException) {
            onMKeyResponseError()
        }
    }

    private fun register() {
        activationState = MKActivationState.REGISTERING
        try {
            claySDK?.publicKey?.let { publicKey ->
                deviceService.registerDevice(
                    DeviceInfo.model,
                    DeviceInfo.uniquePseudoID(context),
                    publicKey
                )
            }
        } catch (e: ClayException) {
            onMKeyResponseError()
        }
    }

    override fun onGetDeviceListError() {
        register()
    }

    override fun deleteDevice() {
        sharedPrefs.device = null
    }

    // When the normal flow is used (login -> regular digital key), guestDigitalKey should be left empty/null, parameter key is only used for digital key for guests.
    override fun openLock(needsBackground: Boolean, key: GuestDigitalKey?) {
        if (!checkAndAskRequiredPermission(needsBackground)) {
            return
        }
        if (isOpening) {
            return
        }
        isOpening = true
        key?.let {
            // Guest Digital Key Flow;
            try {
                val deviceUid = key.device?.deviceUid ?: return
                val mKeyData = key.mKeyData ?: return
                val guestKeyClaySDK = ClaySDK.init(context, apiKey, deviceUid)
                guestKeyClaySDK?.sendDigitalKey(mKeyData, lockDiscoveryCallback) //Opening start
            } catch (exception: ClayException) {
                handleClayException(exception)
            }
            return
        } ?: run {
            // Default flow (via login);
            sharedPrefs.device?.mKeyData?.let { mKey ->
                try {
                    claySDK?.sendDigitalKey(mKey, lockDiscoveryCallback) //Opening start
                } catch (exception: ClayException) {
                    handleClayException(exception)
                }
                return
            }
        }

        isOpening = false
        view?.onMobileKeyNotFound()
    }

    override fun onResume() {
        if (isBluetoothTurnedOn) {
            scanOnBluetoothTurnedOn(AppConfig.Timers.MKEY_BLUETOOTH_ON_RETRY)
            isBluetoothTurnedOn = false
        }

        context.registerReceiver(
            bluetoothReceiver,
            IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED)
        )
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onRequestPermissionResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray,
        activity: Activity
    ) {
        when {
            grantResults.isEmpty() -> onMissingLocationPermission()

            grantResults[0] == PackageManager.PERMISSION_GRANTED -> {
                isLocationPermissionDenied = false
                view?.onPermissionGranted()
            }

            Build.VERSION.SDK_INT < Build.VERSION_CODES.M -> onMissingLocationPermission()

            permissions.isNotEmpty() && !activity.shouldShowRequestPermissionRationale(permissions[0]) -> {
                isLocationPermissionDenied = true
                view?.onNeverAskLocationPermissionAgain()
            }

            else -> onMissingLocationPermission()
        }
    }

    override fun onGoToSettingsClick() {
        isLocationPermissionDenied = false
    }

    private fun onMissingLocationPermission() {
        isLocationPermissionDenied = true
        view?.onMissingLocationPermission()
    }

    override fun hasLocationPermissions(needsBackground: Boolean): Boolean {
        val locationPermission =
            ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q && needsBackground) {
            val backgroundLocationPermission = ContextCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_BACKGROUND_LOCATION
            )
            return locationPermission == PackageManager.PERMISSION_GRANTED && backgroundLocationPermission == PackageManager.PERMISSION_GRANTED
        }

        return locationPermission == PackageManager.PERMISSION_GRANTED
    }

    private fun hasLocationAndBluetoothPermissions(context: Context?, needsBackground: Boolean): Boolean {
        context?.let {
            return when {
                // If user has Android S or higher, it needs only the new bluetooth permissions - both on-app & from widget;
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.S -> {
                    hasAndroidSBluetoothPermissions(it)
                }
                // If user has Android higher Q or R & is accessing widget, he/she needs background location permission in addition to fine location permission
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q && needsBackground -> {
                    val hasLocationPermission = ContextCompat.checkSelfPermission(it, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    val hasBackgroundLocationPermission = ContextCompat.checkSelfPermission(it, Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED
                    hasLocationPermission && hasBackgroundLocationPermission
                }
                // If user has Android lower than S & is not using widget, he just needs fine location permission.
                else -> ContextCompat.checkSelfPermission(it, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
            }
        } ?: return false
    }

    override fun checkAndAskRequiredPermission(needsBackground: Boolean): Boolean {
        if (isLocationPermissionDenied) {
            view?.onNeverAskLocationPermissionAgain()
            return false
        }

        if (hasLocationAndBluetoothPermissions(context, needsBackground)) {
            return true
        }

        val permissions = when {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.S -> arrayOf(
                Manifest.permission.BLUETOOTH_SCAN,
                Manifest.permission.BLUETOOTH_CONNECT
            )
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q && needsBackground -> arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_BACKGROUND_LOCATION
            )
            else -> arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)
        }

        view?.requestMissingPermissions(
            permissions,
            AppConfig.RequestCodes.REQUEST_FINE_LOCATION_PERMISSION
        )
        return false
    }

    private fun hasAndroidSBluetoothPermissions(context: Context): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            val hasBluetoothScanPermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.BLUETOOTH_SCAN
            ) == PackageManager.PERMISSION_GRANTED
            val hasBluetoothConnectPermission = ContextCompat.checkSelfPermission(context,
                Manifest.permission.BLUETOOTH_CONNECT
            ) == PackageManager.PERMISSION_GRANTED
            return hasBluetoothScanPermission && hasBluetoothConnectPermission
        }
        return true
    }

    override fun onDeviceCertificateUpdated(device: Device?) {
        device?.id?.let {
            downloadMKey(it)
        } ?: run { activationState = MKActivationState.ERROR }
    }

    private fun downloadMKey(deviceId: String) {
        activationState = MKActivationState.DOWNLOADING
        deviceService.getMobileKey(deviceId)
    }

    override fun onMKeyResponseError() {
        activationState = MKActivationState.ERROR
    }

    override fun didReceiveAuthState(state: AuthState?) {
        sharedPrefs.writeAuthState(state)
    }

    override fun didReceiveAuthenticationError() {
        logout()
    }

    override fun didReceiveAuthenticationFailure() {
        logout()
    }
}