package com.salto.claysdkdemo.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.salto.claysdkdemo.R
import com.salto.claysdkdemo.application.AppConfig
import com.salto.claysdkdemo.base.SaltoActivity
import com.salto.claysdkdemo.databinding.ActivityLoginBinding
import com.salto.claysdkdemo.guest_digital_key.GuestDigitalKeysListActivity
import com.salto.claysdkdemo.login.presenters.ILoginPresenter
import com.salto.claysdkdemo.main.MainActivity
import net.openid.appauth.AuthorizationException
import net.openid.appauth.AuthorizationResponse

class LoginActivity : SaltoActivity<ILoginPresenter.View, ILoginPresenter.Action>(), ILoginPresenter.View {

    lateinit var binding : ActivityLoginBinding
    private fun setContentView() {
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView()

        if(presenter.isLoggedIn) {
            onLoginSuccess()
            return
        }

        binding.apply {
            loginButton.apply {
                setOnClickListener {
                    presenter.login()
                }
                visibility = View.VISIBLE
            }
            gdkListButton.apply {
                setOnClickListener {
                    context.startActivity(Intent(context, GuestDigitalKeysListActivity::class.java))
                }
                visibility = View.VISIBLE
            }
        }
    }

    override fun inject() {
        app?.clayComponent?.inject(this)
    }

    override fun bindView() {
        presenter.bindView(this)
    }

    override fun onLoginSuccess() {
        binding.apply {
            loginButton.visibility = View.GONE
            gdkListButton.visibility = View.GONE
        }
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    private fun hideProgress() {
        binding.apply {
            loginButton.isEnabled = true
            gdkListButton.isEnabled = true
            progressBar.visibility = View.GONE
        }
    }

    override fun onLoginError() {
        binding.apply {
            loginButton.isEnabled = true
            gdkListButton.isEnabled = true
        }
        hideProgress()
    }

    override fun displayOpenIDIntent(authIntent: Intent) {
        binding.apply {
            loginButton.isEnabled = false
            gdkListButton.isEnabled = false
            progressBar.visibility = View.VISIBLE
        }
        startActivityForResult(authIntent, AppConfig.RequestCodes.AUTH_CODE)
    }

    override fun displayBrowserError() {
        showMessageDialog(getString(R.string.browser_not_found))
    }

    override fun displayError(error: String) {
        hideProgress()
        showMessageDialog(error)
    }

    override fun onOIDConfigError() {
        showMessageDialog(getString(R.string.verify_oid_config))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == AppConfig.RequestCodes.AUTH_CODE && data != null) {
            presenter.exchangeToken(AuthorizationResponse.fromIntent(data), AuthorizationException.fromIntent(data))
        }
    }
}