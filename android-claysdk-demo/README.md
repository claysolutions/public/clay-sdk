### What is this repository for? ###

This is a sample app that shows how to obtain a token for the SaltoKS's APIs
and activate an Android device to be used to unlock a Salto lock via Mobile Key.

### How do I get set up? ###

In order to download ClaySDK from our private repository, please request a deploy token (or use yours if you already have one).
You can also check guide [here](https://gitlab.com/claysolutions/public/clay-sdk/-/wikis/iOS-ClaySDK-Integration#1-importing-claysdk) on how to integrate ClaySDK.
and use it on the following snippet (inside your module-level build.gradle):

```
repositories {
    ...
    maven {
        name 'Gitlab'
        url 'https://gitlab.com/api/v4/projects/42000646/packages/maven'
        credentials {
            username = '{YOUR_USERNAME}'
            password = '{YOUR_DEPLOY_TOKEN}'
        }
        authentication {
            basic(BasicAuthentication)
        }
    }
    ...
}
```

In order to be able to login to our APIs an integrator is supposed to receive an OpenId client configuration.
This configuration will be specific and unique for any different integrator.

Please replace the configuration values obtained for your client in the strings.xml file where missing.
By default the app will point to the Salto's Connect APIs to their accept environment

```xml
    <string name="client_id"></string>
    <string name="dkg_client_id"></string>
    <string name="appauth_redirect_scheme"></string>
    <string name="redirect_url"></string>
    <string name="logout_redirect_url"></string>
    <string name="identity_server_url">https://clp-accept-identityserver.saltoks.com</string>
    <string name="public_api_key">MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEFYDlLVhKz+qNQIBASs322cib/iwnnuSWczXSvU8GGYB6pgZgaCroCywHMPclFRehVsB+jYRJd6n4zkhDSGd5bQ==</string>
    <string name="connect_api_url">https://clp-accept-user.saltoks.com</string>
```

  
