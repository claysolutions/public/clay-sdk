package com.yourapp;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.myclay.claysdk.api.ClaySDK;
import com.myclay.claysdk.api.IClaySDK;
import com.myclay.claysdk.api.ILockDiscoveryCallback;
import com.myclay.claysdk.api.error.ClayException;
import com.saltosystems.justinmobile.sdk.common.OpResult;
import com.saltosystems.justinmobile.sdk.model.Result;

import org.jetbrains.annotations.NotNull;

import java.util.Random;

// Methods implemented for modules exposed to React Native are annotated with
// `@ReactMethod` like below
public class RNSaltoModule extends ReactContextBaseJavaModule {
    private final ReactApplicationContext context;
    private IClaySDK clayInstance;

    RNSaltoModule(ReactApplicationContext context) {
        super(context);
        this.context = context;
    }

    @NotNull
    @Override
    public String getName() {
        return "RNSaltoModule";
    }

    @ReactMethod
    public void getPublicKey(String installationUID, String publicApiKey, final Promise promise) {
        if (installationUID == null) {
            promise.reject("RNSaltoGenericError", "Empty installation ID");
            return;
        }
        if (publicApiKey == null) {
            promise.reject("RNSaltoGenericError", "Empty public API key");
            return;
        }
        try {
            this.clayInstance = ClaySDK.init(this.context, publicApiKey, installationUID);
            promise.resolve(this.clayInstance.getPublicKey());
        } catch (ClayException e) {
            promise.reject("RNSaltoClayError", e.getMessage(), e);
        }
    }

    @ReactMethod
    public void openDoor(String installationUID, String publicApiKey, String mobileKey, final Promise promise) {
        if (installationUID == null) {
            promise.reject("RNSaltoGenericError", "Empty installation ID");
            return;
        }
        if (publicApiKey == null) {
            promise.reject("RNSaltoGenericError", "Empty public API key");
            return;
        }
        if (mobileKey == null) {
            promise.reject("RNSaltoGenericError", "Empty mobile key");
            return;
        }

        try {
            if (this.clayInstance == null) {
                this.clayInstance = ClaySDK.init(this.context, publicApiKey, installationUID);
            }

            this.clayInstance.openDoor(mobileKey, new ILockDiscoveryCallback() {
                @Override
                public void onPeripheralFound() {}

                @Override
                public void onSuccess(Result result) {
                    int resultCode = result.getOpResult();
                    switch (OpResult.getGroup(resultCode)) {
                        case ACCEPTED:
                            promise.resolve(null);
                            break;
                        case FAILURE:
                        case REJECTED:
                        case UNKNOWN_RESULT:
                            promise.reject("RNSaltoDoorError", "A problem occurred with the key");
                    }
                }

                @Override
                public void onFailure(ClayException exception) {
                    promise.reject("RNSaltoDoorError", exception);
                }
            });
        } catch (ClayException e) {
            promise.reject("RNSaltoClayError", e);
        }
    }
}
