## React Native
`SaltoAPI.js` contains the functions that calls Salto's endpoints to do a bunch
of operations like registering a user. `SaltoHelper.js` is the main file that
contains all of the required functions for the main app to interface with Salto,
and is also the file that consumes and uses the native module defined in native
iOS and Android.


## iOS
The module that acts as the interface between native iOS and React Native (JS)
is in `ios/RNSaltoModule.m`, where we define the name of the module and the two
methods that the module is going to implement. `ios/RNSaltoModule.swift` is the
file implementing the methods that utilise Salto's SDK. More instructions on
where to integrate these files in a React Native project:
https://reactnative.dev/docs/0.67/native-modules-ios


## Android
The main module utilising Salto's SDK is `android/RNSaltoModule.java`, and the
other accompanying file (`android/MyRNPackage.java`) is to expose the module to
React Native (by registering this package in your app's `MainApplication.java`).
More instructions on how to integrate these into a native module for Android:
https://reactnative.dev/docs/0.67/native-modules-android
