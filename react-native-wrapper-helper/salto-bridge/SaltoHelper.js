/**
 * This file contains all of the functions that handle operations regarding the
 * usage of Salto's SDK, including registering the device, replacing expired
 * keys, and fetching the MKey to be used for unlocking doors. The objective is
 * to simplify how the main app interfaces with Salto, abstracting away all of
 * these operations as much as possible from the app.
 * 
 * There are some baked-in assumptions like using the Keychain to store the MKey
 * and using the logged-in user's ID and current device ID to form the
 * installation UID for the SDK.
 */

// The object containing the native module that we have exposed to React Native
import { NativeModules } from "react-native";

// Helper functions to get the device's name and ID to be used for registration
import { getDeviceName, getUniqueId } from "react-native-device-info";

// Helper package to handle dates and times
import { dayjs } from "dayjs";

// Retrieve these from some secure storage like .env
import { SALTO_AUTH_CODE_CLIENT_ID, SALTO_PUBLIC_API_KEY_ACCEPT } from "@env";

// In this example, the MKey and its expiry date are stored in the device's
// Keychain
import {
  KeychainManager,
  KeychainService,
} from "your-storage-option/KeychainManager";

// UserSession containing your user's ID to be used as Salto installation ID
import { UserSession } from "your-user-session/UserSession";

// Functions to interface with Salto's server
import { SaltoAPI } from "./SaltoAPI";

// Default number of days before a certificate's actual expiry for it to start
// getting considered as "expired"
const SALTO_CERT_EXPIRY_BUFFER_DAYS = 30;

export const SaltoHelper = {
  /**
   * This method attempts to store the Salto MKey required to unlock doors by
   * first checking if the device has been registered before. If it has, it
   * proceeds to fetch its MKey and store it and its expiry date onto the
   * Keychain. If it has not, it first registers the device and then fetch its
   * MKey and store it onto the Keychain. Returns the logged-in user.
   *
   * Note: this method throws, so wrap it around a catch block.
   */
  async tryAuthenticate({ code, codeVerifier, redirectUrl }) {
    const accessToken = await SaltoAPI.getAccessToken({
      code,
      codeVerifier,
      redirectUrl,
      clientId: SALTO_AUTH_CODE_CLIENT_ID,
    });

    // handle auth depending on whether device has previously been registered or not
    const registeredDevice = await _checkDeviceRegistration(accessToken);
    const saltoPublicKey = await _getSaltoPublicKey();

    let authResults;
    if (registeredDevice) {
      authResults = await _authRegisteredDevice({
        accessToken,
        saltoPublicKey,
        registeredDevice,
      });
    } else {
      authResults = await _authNewDevice(accessToken, saltoPublicKey);
    }

    // store the results of successfully authenticating with Salto onto Keychain
    await KeychainManager.store({
      key: KeychainService.SaltoMKey,
      value: authResults.mkey,
    });
    await KeychainManager.store({
      key: KeychainService.SaltoDeviceId,
      value: authResults.deviceId,
    });
    await KeychainManager.store({
      key: KeychainService.SaltoExpiryDate,
      value: authResults.mkeyExpiryDate,
    });
  },

  /**
   * Fetches the Salto mkey that's normally stored in the Keychain to determine
   * if the user has authenticated with Salto before or not. If they key doesn't
   * exist, the user has not yet been authenticated.
   *
   * @returns whether the user has authenticated with Salto previously
   */
  async tryHasAuthenticated() {
    const mkey = await KeychainManager.retrieve(KeychainService.SaltoMKey);
    return !!mkey;
  },

  /**
   * If provided an expiry date argument, this method returns whether it is
   * within the buffer day count of expiration. Otherwise, it attempts to fetch
   * the `SaltoExpiryDate` and `SaltoMKey` services from the Keychain and check
   * that date instead.
   *
   * Throws if no arguments are given and no expiry date and mkey is found on
   * the Keychain.
   */
  async tryHasMkeyExpired(mkeyExpiryDate) {
    let expiryDate = mkeyExpiryDate;
    if (!expiryDate) {
      expiryDate = await KeychainManager.retrieve(
        KeychainService.SaltoExpiryDate
      );

      const mkey = await KeychainManager.retrieve(KeychainService.SaltoMKey);
      if (!mkey) {
        throw new Error("No key has been stored on the device.");
      }
    }

    if (!expiryDate) {
      throw new Error("No key has been stored on the device.");
    }

    // mkey has "expired" if it's currently within the specified buffer days
    return dayjs().isSameOrAfter(
      dayjs(expiryDate).subtract(SALTO_CERT_EXPIRY_BUFFER_DAYS, "day")
    );
  },

  /** Tries to unlock door using the MKey stored on the Keychain. */
  async tryUnlockDoor() {
    await _unlockDoorNative();
  },
};

/**
 * The actual exposed native Salto module which implements the promised
 * following methods:
 *
 * - ```
 * async getPublicKey(
 *     installationUID: String,
 *     publicApiKey: String,
 * ) throws -> String
 * ```
 *
 * - ```
 * async openDoor(
 *     installationUID: String,
 *     publicApiKey: String,
 *     mobileKey: String,
 * ) throws
 * ```
 */
const _RNSaltoModule = NativeModules.RNSaltoModule;

/** Helper function to check if current device has been registered before. */
async function _checkDeviceRegistration(accessToken) {
  const deviceUniqueId = getUniqueId();
  const devices = await SaltoAPI.getUserDevices({ accessToken });
  return devices.find((d) => d.device_uid === deviceUniqueId);
}

/** Helper function to fetch the MKey for this already-registered device. */
async function _authRegisteredDevice({
  accessToken,
  saltoPublicKey,
  registeredDevice,
}) {
  let mkeyExpiryDate = registeredDevice.mkey.expiry_date;

  const mKeyHasExpired = await SaltoHelper.tryHasMkeyExpired(mkeyExpiryDate);

  // if the registered device's previous mkey has expired, get a new one
  if (mKeyHasExpired) {
    mkeyExpiryDate = await SaltoAPI.replaceUserCert({
      accessToken,
      id: registeredDevice.id,
      publicKey: saltoPublicKey,
    });
  }

  // obtain the mkey for this device
  const mkey = await SaltoAPI.getMKeyWithId({
    accessToken,
    id: registeredDevice.id,
  });

  return { mkey, mkeyExpiryDate, deviceId: registeredDevice.id };
}

/** Helper function to register the current device and fetch its MKey. */
async function _authNewDevice(accessToken, saltoPublicKey) {
  const deviceName = await getDeviceName();
  const deviceUniqueId = getUniqueId();

  // register the device first
  const { id, expiryDate } = await SaltoAPI.registerUserDevice({
    accessToken,
    publicKey: saltoPublicKey,
    deviceName,
    deviceUid: deviceUniqueId,
  });

  // obtain the newly-registered device's mkey
  const mkey = await SaltoAPI.getMKeyWithId({
    accessToken,
    id,
  });

  return { mkey, mkeyExpiryDate: expiryDate, deviceId: id };
}

/** Helper function to call the native module to unlock the door. */
async function _unlockDoorNative() {
  const mkey = await KeychainManager.retrieve(KeychainService.SaltoMKey);
  const installationId = _getSaltoInstallationId();
  await _RNSaltoModule.openDoor(
    installationId,
    SALTO_PUBLIC_API_KEY_ACCEPT,
    mkey
  );
}

/** Helper function to call the native module to get the public key. */
async function _getSaltoPublicKey() {
  const saltoInstallationId = _getSaltoInstallationId();
  return _RNSaltoModule.getPublicKey(
    saltoInstallationId,
    SALTO_PUBLIC_API_KEY_ACCEPT
  );
}

/** Helper function to form the user-device combination as installation ID. */
function _getSaltoInstallationId() {
  const userId = UserSession.getUser()?.id.toString();
  const deviceUniqueId = getUniqueId();
  return `${userId ?? ""}-${deviceUniqueId}`;
}
