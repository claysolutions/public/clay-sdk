//
//  RNSaltoModule.swift
//

import ClaySDK
import Foundation
import SaltoJustINMobileSDK

enum RNSaltoError: String {
  case generic = "RNSaltoGenericError"
  case clay = "RNSaltoClayError"
  case door = "RNSaltoDoorError"
  case ble = "RNSaltoBLEError"
}

@objc(RNSaltoModule)
class RNSaltoModule: NSObject, ClayDelegate, OpenDoorDelegate {
  var clayInstance: Clay!
  var clayInitErrorHandler: RCTPromiseRejectBlock!

  var openDoorSuccessHandler: RCTPromiseResolveBlock!
  var openDoorErrorHandler: RCTPromiseRejectBlock!

  // ClayDelegate protocol method
  func didReceive(error: Error) {
    if openDoorErrorHandler != nil {
      openDoorErrorHandler(RNSaltoError.clay.rawValue, error.localizedDescription, error)
    } else if clayInitErrorHandler != nil {
      clayInitErrorHandler(RNSaltoError.clay.rawValue, error.localizedDescription, error)
    }
  }

  /* OpenDoorDelegate protocol methods */

  func didFindLock() {}

  func didOpen(with result: ClayResult?) {
    guard let result = result else { return }

    let group = SSOpResult.getGroup(result.getOpResult())
    switch group {
    case .groupAccepted:
      // key sucessfully sent to lock (we don't know if user have access, access is indicated by light of the lock)
      openDoorSuccessHandler(nil)
    case .groupFailure, .groupRejected, .groupUnknownResult:
      // there was a problem with sending key to the lock
      openDoorErrorHandler(RNSaltoError.door.rawValue, "A problem occurred with the key", nil)
    default:
      break
    }
  }

  func didReceiveTimeout() {
    openDoorErrorHandler(RNSaltoError.door.rawValue, "Open door process timed out", nil)
  }

  func alreadyRunning() {}

  func didReceiveBLE(error: Error) {
    openDoorErrorHandler(RNSaltoError.ble.rawValue, error.localizedDescription, error)
  }

  /* End of OpenDoorDelegate protocol methods */

  /// Method to initialise the Clay SDK with an installation ID provided by JS-land. Rejects if the installation ID
  /// is nil. Otherwise, resolves with a public key if the SDK is initialised successfully.
  @objc
  func getPublicKey(
    _ installationUID: String?,
    publicApiKey: String?,
    resolve: RCTPromiseResolveBlock,
    reject: @escaping RCTPromiseRejectBlock
  ) {
    guard let installationUID = installationUID else {
      reject(RNSaltoError.generic.rawValue, "Empty installation ID", nil)
      return
    }

    guard let publicApiKey = publicApiKey else {
      reject(RNSaltoError.generic.rawValue, "Empty public API key", nil)
      return
    }

    clayInitErrorHandler = reject
    clayInstance = Clay(
      installationUID: installationUID,
      apiKey: publicApiKey,
      delegate: self
    )
    resolve(clayInstance.getPublicKey())
  }

  /// Method to open the lock using an encrypted mobile key provided by JS-land. Rejects if the mobile key
  /// is nil.
  @objc
  func openDoor(
    _ installationUID: String?,
    publicApiKey: String?,
    mobileKey: String?,
    resolve: @escaping RCTPromiseResolveBlock,
    reject: @escaping RCTPromiseRejectBlock
  ) {
    guard let installationUID = installationUID else {
      reject(RNSaltoError.generic.rawValue, "Empty installation ID", nil)
      return
    }

    guard let publicApiKey = publicApiKey else {
      reject(RNSaltoError.generic.rawValue, "Empty public API key", nil)
      return
    }

    guard let mobileKey = mobileKey else {
      reject(RNSaltoError.generic.rawValue, "Empty mobile key", nil)
      return
    }

    if (clayInstance == nil) {
      clayInstance = Clay(
        installationUID: installationUID,
        apiKey: publicApiKey,
        delegate: self
      )
    }

    openDoorSuccessHandler = resolve
    openDoorErrorHandler = reject
    clayInstance.openDoor(with: mobileKey, delegate: self)
  }
}
