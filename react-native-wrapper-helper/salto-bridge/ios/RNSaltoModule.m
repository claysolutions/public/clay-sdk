//
//  RNSaltoModule.m
//

#import <React/RCTBridgeModule.h>

// This defines the module to be exposed to React Native
@interface RCT_EXTERN_MODULE (RNSaltoModule, NSObject)

// These specify the methods that are callable on the module
RCT_EXTERN_METHOD(getPublicKey:(NSString *)installationUID
                  publicApiKey:(NSString *)publicApiKey
                  resolve:(RCTPromiseResolveBlock)resolve
                  reject:(RCTPromiseRejectBlock)reject)

RCT_EXTERN_METHOD(openDoor:(NSString *)installationUID
                  publicApiKey:(NSString *)publicApiKey
                  mobileKey:(NSString *)mobileKey
                  resolve:(RCTPromiseResolveBlock)resolve
                  reject:(RCTPromiseRejectBlock)reject)

@end
