The use of the third-party software links on this website is done at your own discretion and risk and with agreement that you will be solely responsible for any damage to your computer system or loss of data that results from such activities. You are solely responsible for adequate protection and backup of the data and equipment used in connection with any of the software linked to this website, and we will not be liable for any damages that you may suffer connection with downloading, installing, using, modifying or distributing such software. No advice or information, whether oral or written, obtained by you from us or from this website shall create any warranty for the software.

 

Additionally, we make no warranty that:

The third-party software will meet your requirements.
The third-party software will be uninterrupted, timely, secure or error-free.
The results from the use of the third-party software will be effective, accurate or reliable.
The quality of the third-party software will meet your expectations.
If errors or problems occur in connection with a download of the third-party software obtained from the links on this website, they will be corrected.
The links to third-party software and the related documentation made available on this website are subject to the following conditions:

The software could include technical or other mistakes, inaccuracies or typographical errors.
At any time without prior notice, we may make changes to the links pointing to third-party software or documentation made available on the third-party's website.
The software may be out of date, and we make no commitment to update such materials.
We assume no responsibility for errors or omissions in the third-party software or documentation available from its website.
In no event shall we be liable to you or any third parties for any special, punitive, incidental, indirect or consequential damages of any kind, or any damages whatsoever, including, without limitation, those resulting from loss of use, lost data or profits, or any liability, arising out of or in connection with the use of this third-party software.
