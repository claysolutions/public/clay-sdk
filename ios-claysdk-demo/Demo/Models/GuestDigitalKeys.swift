//
//  GuestDigitalKeys.swift
//  Demo
//
//  Created by Jakov Videkovic on 07/02/2022.
//

import Foundation

class GuestDigitalKeys: Codable {
    var items: [GuestDigitalKey] = []
}

class GuestDigitalKey: Codable {
    var device: Device?
    var firstName: String?
    var lastName: String?
    var dateCreated: Date?
    var digitalKey: MobileKeyData?
}
