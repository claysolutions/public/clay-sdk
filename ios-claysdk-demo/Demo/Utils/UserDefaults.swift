//
//  UserDefaults.swift
//  Demo
//
//  Created by Jakov Videkovic on 09/12/2020.
//

import Foundation

@propertyWrapper
struct UserDefault<Value> {
    let key: Keys
    let defaultValue: Value
    var container: UserDefaults = .standard
    
    enum Keys: String {
        case accessToken, mkeyData
    }

    var wrappedValue: Value {
        get {
            return container.object(forKey: key.rawValue) as? Value ?? defaultValue
        }
        set {
            container.set(newValue, forKey: key.rawValue)
        }
    }
}

@propertyWrapper
struct UserDefaultNSCoding<Value> where Value: NSObject, Value: NSSecureCoding {
    
    enum Keys: String {
        case serviceConfiguration, state
    }
    
    let key: Keys
    var container: UserDefaults = .standard

    var wrappedValue: Value? {
        get {
            guard let data = container.data(forKey: key.rawValue) else {
                return nil
            }
            return try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? Value
        }
        set {
            guard let value = newValue else {
                container.removeObject(forKey: key.rawValue)
                return
            }
            guard let data = try? NSKeyedArchiver.archivedData(withRootObject: value, requiringSecureCoding: false) else { return }

            container.set(data, forKey: key.rawValue)
        }
    }
}


@propertyWrapper
struct UserDefaultCodable<Value> where Value: Codable {
    
    enum Keys: String {
        case device, mobileKey, guestDigitalKeys
    }
    
    let key: Keys
    var container: UserDefaults = .standard

    var wrappedValue: Value? {
        get {
            guard let data = container.data(forKey: key.rawValue) else {
                return nil
            }
            return try? JSONDecoder().decode(Value.self, from: data)
        }
        set {
            guard let value = newValue else {
                container.removeObject(forKey: key.rawValue)
                return
            }
            guard let data = try? JSONEncoder().encode(value) else { return }

            container.set(data, forKey: key.rawValue)
        }
    }
}
