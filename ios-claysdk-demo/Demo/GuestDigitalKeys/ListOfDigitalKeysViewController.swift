//
//  ListOfDigitalKeysViewController.swift
//  Demo
//
//  Created by Jakov Videkovic on 02/02/2022.
//

import Foundation
import UIKit

class ListOfDigitalKeysViewController: UIViewController {
    
    @IBOutlet weak var dkeyTableView: UITableView!
    
    @UserDefaultCodable(key: .guestDigitalKeys)
    private var guestDigitalKeys: GuestDigitalKeys?
    
    private var items: [GuestDigitalKey] {
        guestDigitalKeys?.items.sorted(by: { $0.dateCreated ?? Date() > $1.dateCreated ?? Date() }) ?? []
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        dkeyTableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let cell = sender as? ListOfDigitalKeysCell,
              let item = cell.item else { return }
        if let destVC = segue.destination as? SendDigitalKeyViewController {
            destVC.installationId = item.device?.deviceUID ?? ""
            destVC.mkeyData = item.digitalKey?.mKeyData ?? ""
        }
    }
}

extension ListOfDigitalKeysViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "listOfDigitalKeys", for: indexPath) as? ListOfDigitalKeysCell else {
            return UITableViewCell()
        }
        let item = items[indexPath.row]
        cell.nameLbl.text = "\(item.firstName ?? "A") \(item.lastName ?? "Person")"
        cell.deviceLbl.text = item.device?.deviceName
        if #available(iOS 15.0, *) {
            cell.dateLbl.text = item.dateCreated?.formatted(date: .abbreviated, time: .standard)
        } else {
            cell.dateLbl.text = item.dateCreated?.description
        }
        cell.item = item
        return cell
    }
}


class ListOfDigitalKeysCell: UITableViewCell {
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var deviceLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    
    var item: GuestDigitalKey?
}
