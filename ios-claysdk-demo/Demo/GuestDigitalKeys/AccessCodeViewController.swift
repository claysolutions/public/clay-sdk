//
//  AccessCodeViewController.swift
//  Demo
//
//  Created by Jakov Videkovic on 02/02/2022.
//

import Foundation
import UIKit

class AccessCodeViewController: UIViewController {
    
    @IBOutlet weak var accessCodeField: UITextField!
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var errorMessageLabel: UILabel!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var saveButton: UIButton!
    
    let presenter = AccessCodePresenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.view = self
    }
    
    @IBAction func didTapSave(_ sender: Any) {
        guard let accessCode = accessCodeField.text, !accessCode.isEmpty,
              let firstName = firstNameField.text,
              let lastName = lastNameField.text else { return }
        
        presenter.didTapSave(accessCode: accessCode, firstName: firstName, lastName: lastName)
        loader.startAnimating()
        saveButton.isHidden = true
    }
    
}

extension AccessCodeViewController: AccessCodeViewProtocol {
    func showError(message: String) {
        loader.stopAnimating()
        saveButton.isHidden = false
        errorMessageLabel.text = message
    }
    
    func didFinishActivation() {
        navigationController?.popViewController(animated: true)
    }
}
