//
//  AccessCodePresenter.swift
//  Demo
//
//  Created by Jakov Videkovic on 02/02/2022.
//

import Foundation
import ClaySDK
import UIKit

protocol AccessCodeViewProtocol: AnyObject {
    
    func showError(message: String)
    
    func didFinishActivation()
}

class AccessCodePresenter {
    
    @PropertyList(key: .configuration)
    private var configuration: Configuration
    
    @UserDefaultCodable(key: .guestDigitalKeys)
    private var guestDigitalKeys: GuestDigitalKeys?
    
    private lazy var deviceUID: String = {
        UUID().uuidString
    }()
    
    private lazy var claySDK: Clay? = {
        do {
            return try Clay(installationUID: deviceUID, apiKey: configuration.apiPublicKey!)
        } catch {
            view?.showError(message: error.localizedDescription)
            return nil
        }
    }()
    
    
    weak var view: AccessCodeViewProtocol?
    
    private let authSevice = AuthService()
    private let podService = PodService()
    private let deviceService = DeviceService()
    
    private let guestDigitalKey = GuestDigitalKey()
    
    func didTapSave(accessCode: String, firstName: String, lastName: String) {
        authSevice.fetchAccessTokenFor(accessCode: accessCode) { result in
            switch result {
            case .success:
                self.registerPodGuest(firstName: firstName, lastName: lastName)
            case .failure(let error):
                self.view?.showError(message: error.localizedDescription)
            }
            
        }
    }
    
    private func registerPodGuest(firstName: String, lastName: String) {
        podService.registerPodGuest(firstName: firstName, lastName: lastName) { [self] result in
            switch result {
            case .success:
                self.guestDigitalKey.firstName = firstName
                self.guestDigitalKey.lastName = lastName
                self.registerDevice()
            case .failure(let error):
                self.view?.showError(message: error.localizedDescription)
            }
        }
    }
    
    private func registerDevice() {
        guard let publicKey = claySDK?.getPublicKey() else {
            return
        }
        deviceService.registerDevice(
            deviceName: UIDevice.current.name,
            deviceUID: deviceUID,
            publicKey: publicKey
        ) { result in
            switch result {
            case .success(let device):
                self.guestDigitalKey.device = device
                self.downloadMobileKey(for: device)
            case .failure(let error):
                self.view?.showError(message: error.localizedDescription)
            }
        }
    }
    
    private func downloadMobileKey(for device: Device) {
        deviceService.downloadMobileKey(deviceId: device.id) { result in
            switch result {
            case .success(let mkeyData):
                self.guestDigitalKey.digitalKey = mkeyData
                self.guestDigitalKey.dateCreated = Date()
                self.saveInfo()
            case .failure(let error):
                self.view?.showError(message: error.localizedDescription)
            }
        }
    }
    
    private func saveInfo() {
        let activeKeys = guestDigitalKeys ?? GuestDigitalKeys()
        
        activeKeys.items.append(guestDigitalKey)
        
        guestDigitalKeys = activeKeys
        
        view?.didFinishActivation()
    }
}
