//
//  SendDigitalKeyViewController.swift
//  Demo
//
//  Created by Jakov Videkovic on 07/02/2022.
//

import Foundation
import UIKit
import ClaySDK

class SendDigitalKeyViewController: UIViewController {
    var installationId: String = ""
    var mkeyData: String = ""
    
    @IBOutlet weak var statusLbl: UILabel!
    
    @PropertyList(key: .configuration)
    private var configuration: Configuration
    
    private lazy var claySDK: Clay? = {
        do {
            return try Clay(installationUID: installationId, apiKey: configuration.apiPublicKey!)
        } catch {
            statusLbl.text = "Initialization error: \(error.localizedDescription)"
            return nil
        }
    }()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        do {
            try claySDK?.sendDigitalKey(with: mkeyData, delegate: self)
        } catch {
            print("initialisation error: \(error)")
        }
    }
}

extension SendDigitalKeyViewController: DigitalKeyDelegate {

    func onLockFound() {
        statusLbl.text = "Lock found"
    }

    func onSuccess(_ result: ClaySDK.ClayResult, message: String?) {
        switch result {
        case .success:
            statusLbl.text = "Digital Key received"
        case .cancelled:
            statusLbl.text = "Digital Key needs to be reactivated"
        case .failure:
            statusLbl.text = "Something went wrong: \(message ?? "No reason found")"
        @unknown default:
            statusLbl.text = "Something went terribly wrong"
        }
    }

    func onFailure(_ error: ClaySDK.ClayError) {
        switch error {
        case .processAlreadyRunningError:
            statusLbl.text = "Mobile key already running"
        case .timeoutReachedError:
            statusLbl.text = "Lock not found, timeout"
        case .bluetoothNotAuthorizedError, .bluetoothNotSupportedError, .bluetoothNotInitializedError, .bluetoothFeatureNotEnabledError:
            statusLbl.text = "BLE error: \(error.localizedDescription)"
        default:
            statusLbl.text = error.localizedDescription
        }
    }
}
