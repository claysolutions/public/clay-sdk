//
//  WatchManager.swift
//  Demo
//
//  Created by Jakov Videkovic on 10/10/2022.
//
import Foundation
import WatchConnectivity

class WatchManager: NSObject {
    
    private let session = WCSession.default
    
    private let deviceService = DeviceService()
    
    private var activationCompletion: (()->Void)?
    
    func isWatchAvailable() -> Bool {
        session.isPaired && session.isWatchAppInstalled && session.isReachable
    }
    
    func activateKeyWith(completion: @escaping () -> Void) {
        self.activationCompletion = completion
        startConnectingToWatch()
    }
    
    private func startConnectingToWatch() {
        if WCSession.isSupported() {
            session.delegate = self
            session.activate()
        }
    }
    
    private func registerDeviceWith(name: String, publicKey: String, deviceUID: String) {
        deviceService.registerDevice(deviceName: name, deviceUID: UUID().uuidString, publicKey: publicKey) { result in
            switch result {
            case .success(let device):
                self.downloadMobileKey(for: device)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    private func downloadMobileKey(for device: Device) {
        deviceService.downloadMobileKey(deviceId: device.id) { result in
            switch result {
            case .success(let mkeyData):
                // send key back to watch
                self.session.sendMessage(["mkey":mkeyData.mKeyData], replyHandler: nil)
                self.activationCompletion?()
            case .failure(let error):
                print(error)
            }
        }
    }
}
// MARK: SessionDelegate
extension WatchManager: WCSessionDelegate {
    func sessionDidBecomeInactive(_ session: WCSession) {
        print("phone-sessionDidBecomeInactive")
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        print("phone-sessionDidDeactivate")
    }
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        print("phone-activationDidCompleteWith \(activationState.rawValue)")
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String: Any], replyHandler: @escaping ([String: Any]) -> Void) {
        print("phone-didReceiveMessage \(message)")
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String: Any]) {
        print("phone-didReceiveMessageWithoutReply \(message)")
        if let name = message["name"] as? String, let publicKey = message["publicKey"] as? String, let deviceUid = message["deviceUID"] as? String {
            registerDeviceWith(name: name, publicKey: publicKey, deviceUID: deviceUid)
        }
    }
    
    func session(_ session: WCSession, didReceiveMessageData messageData: Data, replyHandler: @escaping (Data) -> Void) {
        print("phone-didReceiveMessageData \(messageData)")
    }
}
