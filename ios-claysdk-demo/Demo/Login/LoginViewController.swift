//
//  ViewController.swift
//  Example
//
//  Created by Jakov Videkovic on 03/12/2020.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var listOfDKeysButton: UIButton!
    
    let presenter = LoginPresenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.view = self
        presenter.discoverConfiguration()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if presenter.isLoggedIn {
            goToMainViewController()
        }
    }

    @IBAction func didTapLogin(_ sender: Any) {
        presenter.login(viewController: self)
    }
    
    @IBAction func didTapListOfDKeys(_ sender: Any) {
        performSegue(withIdentifier: "listSeque", sender: self)
    }
}

extension LoginViewController: LoginViewProtocol {
    func showError(message: String) {
        errorLabel.text = message
    }
    
    func toggleLoginButton(enabled: Bool) {
        loginButton.isEnabled = enabled
    }
    
    func goToMainViewController() {
        performSegue(withIdentifier: "mainSegue", sender: self)
    }
}

