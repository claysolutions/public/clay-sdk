//
//  PodService.swift
//  Demo
//
//  Created by Jakov Videkovic on 04/02/2022.
//

import Foundation

class PodService {
    
    private let networkService: NetworkService
    
    init(networkService: NetworkService = NetworkService()) {
        self.networkService = networkService
    }
    
    
    func registerPodGuest(firstName: String, lastName: String, completion: @escaping (Result<EmptyResponse, Error>) -> Void) {
        let paramsDict: [String: Any] = [
            "first_name": firstName,
            "last_name": lastName
        ]
        
        networkService.request(endpoint: "/pods/guests", httpMethod: "POST", params: paramsDict) { (result: Result<EmptyResponse, Error>) in
            completion(result)
        }
    }
    
}
