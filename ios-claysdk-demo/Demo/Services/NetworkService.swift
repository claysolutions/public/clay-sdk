//
//  NetworkService.swift
//  Demo
//
//  Created by Jakov Videkovic on 11/12/2020.
//

import Foundation
import AppAuth

class NetworkService {
    
    @UserDefaultNSCoding(key: .state)
    private var state: OIDAuthState?
    
    @PropertyList(key: .configuration)
    private var configuration: Configuration
    
    @UserDefault(key: .accessToken, defaultValue: "")
    private var accessToken: String
    
    func request<T: Codable>(endpoint: String, httpMethod: String, params: [String: Any]? = nil, completion: @escaping (Result<T, Error>) -> Void) {
        var request = URLRequest(url: URL(string: "\(configuration.apiUrl)\(endpoint)")!)
        
        if let params = params {
            request = getRequestWith(params: params, for: httpMethod, from: request)
        }
        request.httpMethod = httpMethod
        
        // user is logged in via IDS page and state keeps auth updated
        if let state = self.state {
            state.performAction(freshTokens: { (accessToken, _, error) in
                if let _ = error {
                    completion(.failure("API tokens expired, you can still use mobile key"))
                    return
                }
                // save new state
                self.state = state
                guard let accessToken = accessToken else { return }
                // set required headers
                request.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                self.executeApiRequestWith(request: request, completion: completion)
            })
            return
        }
        
        // user is authenicated via one time token request
        if !accessToken.isEmpty {
            request.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            self.executeApiRequestWith(request: request, completion: completion)
            return
        }
        
        
        completion(.failure("No authorisation provided"))
    }
    
    private func executeApiRequestWith<T: Codable>(request: URLRequest, completion: @escaping (Result<T, Error>) -> Void) {
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            // background thread, we should receive callbacks on main thread
            let mainThreadCompletion: ((Result<T, Error>) -> Void) = { result in
                DispatchQueue.main.async {
                    completion(result)
                }
            }
            
            // error while executing network call
            if let error = error {
                print(error)
                mainThreadCompletion(.failure(error))
                return
            }
            
            guard let response = response as? HTTPURLResponse else { return }
            guard var data = data else { return }
            
            
            // print full response from server
            print(response.url?.absoluteURL ?? "")
            if let json = try? JSONSerialization.jsonObject(with: data, options: []) {
                print("Response: \(json)")
            } else {
                // handle empty response
                data = "{}".data(using: .utf8)!
            }
            
            switch response.statusCode {
            case 200..<300:
                do { // try to decode successful network response
                    let model = try JSONDecoder().decode(T.self, from: data)
                    mainThreadCompletion(.success(model))
                } catch {
                    mainThreadCompletion(.failure(error))
                }
            case 400..<500:
                do { // try to decode error from server
                    let model = try JSONDecoder().decode(SaltoError.self, from: data)
                    mainThreadCompletion(.failure(model.message ?? "Unknown error"))
                } catch {
                    mainThreadCompletion(.failure(error))
                }
            default:
                break
            }
            
        }.resume()
    }
    
    /// Embed params depending on method, GET params are put as query items, POST and PUT serialized to body as json
    private func getRequestWith(params: [String: Any], for httpMethod: String, from request: URLRequest) -> URLRequest {
        var updatedRequest = request
        switch httpMethod {
        case "POST", "PUT":
            updatedRequest.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        case "GET":
            var urlComponents = URLComponents(url: request.url!, resolvingAgainstBaseURL: false)!
            urlComponents.queryItems = params.map({ (key, value) -> URLQueryItem in
                URLQueryItem(name: key, value: value as? String)
            })
            updatedRequest = URLRequest(url: urlComponents.url!)
        default:
            break
        }
        return updatedRequest
    }
}

class SaltoError: Codable {
    var message: String?
    
    enum CodingKeys: String, CodingKey {
        case message = "Message"
    }
}

struct EmptyResponse: Codable {}

extension String: Error {} // Enables us to throw a string

extension String: LocalizedError { // Adds error.localizedDescription to Error instances
    public var errorDescription: String? { return self }
}
