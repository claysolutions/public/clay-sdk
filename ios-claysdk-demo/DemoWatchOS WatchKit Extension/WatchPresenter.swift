//
//  WatchPresenter.swift
//  DemoWatchOS WatchKit Extension
//
//  Created by Jakov Videkovic on 11/10/2022.
//

import Foundation
import WatchConnectivity
import ClaySDK
import WatchKit

class WatchPresenter: NSObject, ObservableObject {
    
    @Published
    var activationActive: Bool = false
    
    @Published
    var openingStatus: String = ""
    
    @UserDefault(key: .mkeyData, defaultValue: "")
    private var mkeyData: String
    
    @PropertyList(key: .configuration)
    private var configuration: Configuration
    
    private let session = WCSession.default
    
    private var claySDK: Clay?
    
    private var deviceUID: String {
        (WKInterfaceDevice.current().identifierForVendor?.uuidString ?? UUID().uuidString)
    }
    
    override init() {
        super.init()
        do {
            claySDK = try Clay(installationUID: deviceUID, apiKey: configuration.apiPublicKey!)
        } catch {
            print("initialisation error: \(error)")
        }
    }
    
    func activateKeyIfNeeded() {
        if mkeyData.isEmpty {
            startActivation()
        }
    }
    
    func sendKey() {
        do {
            try claySDK?.sendDigitalKey(with: mkeyData, delegate: self)
        } catch {
            print("initialisation error: \(error)")
        }
    }
    
    private func startActivation() {
        print("activation started")
        activationActive = true
        if WCSession.isSupported() {
            session.delegate = self
            session.activate()
        }
    }
    
    private func sendMessageToPhone() {
        // get public key from ClaySDK need for key activation
        guard let publicKey = claySDK?.getPublicKey() else { return }
        print("public key: \(publicKey)")
        // session is active and we can send info for activating digital key to mobile device
        session.sendMessage(
            [
                "publicKey": publicKey,
                "name": WKInterfaceDevice.current().name,
                "deviceUID": deviceUID
            ],
            replyHandler: nil) { error in
                print(error)
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    self.sendMessageToPhone()
                }
            }
    }
}

// MARK: SessionDelegate

extension WatchPresenter: WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        print("watch-activationDidCompleteWith \(activationState.rawValue)")
        sendMessageToPhone()
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        print("watch-didReceiveMessage \(message)")
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String: Any]) {
        print("watch-didReceiveMessageWithoutReply \(message)")
        if let mkey = message["mkey"] as? String {
            self.mkeyData = mkey
            self.activationActive = false
        }
    }
}

// MARK: DigitalKeyDelegate

extension WatchPresenter: DigitalKeyDelegate {
    func onLockFound() {
        openingStatus = "Lock found"
    }

    func onSuccess(_ result: ClaySDK.ClayResult, message: String?) {
        switch result {
        case .success:
            openingStatus = "Digital Key received"
        case .cancelled:
            openingStatus = "Digital Key needs to be reactivated"
        case .failure:
            openingStatus = "Something went wrong: \(message ?? "No reason found")"
        @unknown default:
            openingStatus = "Something went terribly wrong"
        }
    }

    func onFailure(_ error: ClaySDK.ClayError) {
        switch error {
        case .processAlreadyRunningError:
            openingStatus = "Mobile key already running"
        case .timeoutReachedError:
            openingStatus = "Lock not found, timeout"
        case .bluetoothNotAuthorizedError, .bluetoothNotSupportedError, .bluetoothNotInitializedError, .bluetoothFeatureNotEnabledError:
            openingStatus = "BLE error: \(error.localizedDescription)"
        default:
            openingStatus = "Error: \(error.localizedDescription)"
        }
    }
}
