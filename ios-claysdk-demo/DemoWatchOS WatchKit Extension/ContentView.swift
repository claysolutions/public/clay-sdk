//
//  ContentView.swift
//  DemoWatchOS WatchKit Extension
//
//  Created by Jakov Videkovic on 05/10/2022.
//

import SwiftUI

struct ContentView: View {
    
    @ObservedObject
    var presenter = WatchPresenter()
    
    var body: some View {
        VStack {
            if presenter.activationActive {
                Text("Activating...")
                ProgressView()
                    .progressViewStyle(CircularProgressViewStyle())
            } else {
                Text(presenter.openingStatus)
                Button("Send key") {
                    presenter.sendKey()
                }
            }
        }.onAppear {
            presenter.activateKeyIfNeeded()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
