//
//  DemoApp.swift
//  DemoWatchOS WatchKit Extension
//
//  Created by Jakov Videkovic on 05/10/2022.
//
import SwiftUI

@main
struct DemoApp: App {
    @SceneBuilder var body: some Scene {
        WindowGroup {
            NavigationView {
                ContentView()
            }
        }
    }
}
